package com.cpe.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cpe.springboot.model.CardModel;

@Repository
public interface CardRepository extends JpaRepository<CardModel, Long>{

}
