$(document).ready(function () {
	createUser = function (name, surname, password, passwordConfirmation) {
		if (password !== passwordConfirmation) {
			alert("Passwords do not match");
			return;
		}
		
		$.ajax({
			url: 'http://localhost:8080/user',
			dataType: 'json',
			type: 'post',
			contentType: 'application/json',
			data: JSON.stringify({"name": name, "username": surname, "password": password}),
			success: function(data, testStatus, jQxhr){
				alert(JSON.stringify(data))
			},
			error: function(jqXhr, textStatus, errorThrown){
				alert(errorThrown);
			}
		});
	};
	
	$("#addUserForm").submit(function() {
		name = $("#addUserForm").find('input[name="name"]').val();
		surname = $("#addUserForm").find('input[name="Surname"]').val();
		password = $("#addUserForm").find('input[name="Password"]').val();
		passwordConfirmation = $("#addUserForm").find('input[name="Re-Password"]').val();
		createUser(name, surname, password, passwordConfirmation);
		return false;
	});
	
	login = function(username, password) {
		$.ajax({
			url: 'http://localhost:8080/user/auth',
			dataType: 'text',
			type: 'post',
			data: username+"&"+password,
			success: function(data, testStatus, jQxhr){
				alert(JSON.stringify(data))
			},
			error: function(jqXhr, textStatus, errorThrown){
				alert(errorThrown);
			}
		});
	}
	
	$("#loginForm").submit(function() {
		username = $("#loginForm").find('input[name="username"]').val();
		password = $("#loginForm").find('input[name="password"]').val();
		login(username, password);
		return false;
	});

});