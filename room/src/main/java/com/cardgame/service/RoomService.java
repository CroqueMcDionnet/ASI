package com.cardgame.service;

import com.cardgame.exception.RoomException;
import com.cardgame.model.*;
import com.cardgame.repository.RoomRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class RoomService {

    @Inject
    private RoomRepository roomRepository;

    @Inject
    private GameService gameService;

    public Room createOrModifyRoom(CreateRoomDto roomDto) {
        Room dbRoom = this.findByName(roomDto.getName());
        if (dbRoom != null && !dbRoom.canBeModified()) {
            throw new RoomException("This room already exists and cannot be modified");
        }

        if (roomDto.getStartBet() <= 0) {
            throw new RoomException("Starting bet must be a strictly positive integer");
        }
        if (roomDto.getName().length() < 5 || roomDto.getName().length() > 150) {
            throw new RoomException("Room name must be at least 5 and no more than 150 characters long");
        }
        Room room = dbRoom == null ? new Room() : dbRoom;

        room.setName(roomDto.getName());
        room.setStartBet(roomDto.getStartBet());

        room.setFirstPlayerCardId(null);
        room.setSecondPlayerCardId(null);

        return this.roomRepository.save(room);
    }

    public boolean exists(String name) {
        return this.findByName(name) != null;
    }

    public Room findByName(String name) {
        return this.roomRepository.findByName(name);
    }

    public void selectCard(int roomId, SelectCardDto selectCardDto) {
        Room room = this.findById(roomId);
        if (room == null) {
            throw new RoomException("Room " + roomId + " doesn't exist");
        }

        // todo call card service to verify it is valid
        // same with player
        if (room.getFirstPlayerId() != null) {
            if (room.getSecondPlayerId() != null) {
                throw new RoomException("The room is full");
            } else {
                room.setSecondPlayerId(selectCardDto.getPlayerId());
                room.setSecondPlayerCardId(selectCardDto.getCardId());
            }
        } else {
            room.setFirstPlayerId(selectCardDto.getPlayerId());
            room.setFirstPlayerCardId(selectCardDto.getCardId());
        }

        room = this.roomRepository.save(room);
        if (room.areBothPlayersReady()) {
            this.createGame(room);
        }
    }

    private void createGame(Room room) {
        this.gameService.create(room.toGetRoomDto());
    }

    private Room findById(int roomId) {
        return this.roomRepository.findById(roomId);
    }

    public GetRoomDto getRoom(int roomId) {
        Room room = this.findById(roomId);
        if (room == null) {
            throw new RoomException("Room " + roomId + " doesn't exist");
        }

        return room.toGetRoomDto();
    }

    public List<Room> findAll() {
        return this.roomRepository.findAll();
    }

    public void designateWinner(int roomId, int winnerPlayerId) {
        Room room = this.findById(roomId);
        if (room == null) {
            throw new RoomException("The room does not exist");
        }
        if (room.getWinner() != null) {
            throw new RoomException("The winner has already been designated");
        }
        if (!room.areBothPlayersReady()) {
            throw new RoomException("The game has not started yet");
        }
        if (room.getFirstPlayerId() == winnerPlayerId) {
            room.setWinner(Winner.FIRST);
        } else if (room.getSecondPlayerId() == winnerPlayerId) {
            room.setWinner(Winner.SECOND);
        } else {
            throw new RoomException("The winner is not in this room");
        }

        // todo call card service to remove card energy ?
    }
}
